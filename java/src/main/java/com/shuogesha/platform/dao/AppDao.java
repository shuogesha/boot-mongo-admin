package com.shuogesha.platform.dao;

import com.shuogesha.platform.entity.App;
import com.shuogesha.platform.web.mongo.MongoBaseDao;

public interface AppDao extends MongoBaseDao<App>{ 
	 
}