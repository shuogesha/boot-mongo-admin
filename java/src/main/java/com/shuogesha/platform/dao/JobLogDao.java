package com.shuogesha.platform.dao;

import com.shuogesha.platform.web.mongo.MongoBaseDao;

import com.shuogesha.platform.entity.JobLog;

public interface JobLogDao extends MongoBaseDao<JobLog> { 
	
 
}