package com.shuogesha.platform.dao;

import com.shuogesha.platform.web.mongo.MongoBaseDao;

import com.shuogesha.platform.entity.ScheduleJob;

public interface ScheduleJobDao extends MongoBaseDao<ScheduleJob> { 
	
 
}