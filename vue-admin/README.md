# vue后台管理系统快速开发

#### 介绍
vue后台管理系统快速开发

---

基于SpringBoot的后台管理系统，实现了系统权限、动态菜单，用户权限，数据字典等基础功能

shop2020官方QQ群1：1067455850
 
**后端相关框架技术**

---

| 框架 | 版本 | 备注 |
| --- | --- | --- |
| jdk | 1.8 |   |
| mysql| 5.6 |  数据库 | 
| springboot | 2.2.2.RELEASE|   | 

**管理相关技术**

---

| 框架 | 版本 | 备注 |
| --- | --- | --- |
| vue | ^2.5.2 |   |
| vue-quill-editor| ^3.0.6 |   |
| vue-router | ^3.0.1|   |
| axios | ^0.19.0 |   |
| element-ui | ^2.13.0|   |
| font-awesome |^4.7.0" |   |
| node-sass | ^4.13.0 |  |
| qs | 3.8 | ^6.9.1 |

**相关开发工具：**

springtoolSute4、visual studio code

**相关docker环境依赖** 

docker pull shuogesha/mysql5.6


后台功能截图

---

![](https://oscimg.oschina.net/oscnet/up-a46a271e94b2762a4fc47f7aee5f4f5b8e3.png)![](https://oscimg.oschina.net/oscnet/up-ee9acc738dd40b04a72307f6894a2c6ad09.png)![](https://oscimg.oschina.net/oscnet/up-f866e82ec41f0ac3bbace3309d9359d9373.png)
如果有用赏个棒棒糖吧

---
![](https://oscimg.oschina.net/oscnet/up-e033c93ff2df3a3cc8bb78ab934c2cf834a.JPEG)![](https://oscimg.oschina.net/oscnet/up-fe67b7c9d60e614536afaeb294d2c795d38.JPEG)