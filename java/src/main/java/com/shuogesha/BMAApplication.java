package com.shuogesha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BMAApplication {

	public static void main(String[] args) {
		SpringApplication.run(BMAApplication.class, args);
	}

}
